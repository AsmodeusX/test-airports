import axios from "axios"
import state from '@/store/index'

export default function(method, data = {}, endpoint) {
    const url_endpoint = `${process.env.VUE_APP_BASEURL}/${endpoint}`

    switch (method) {
        case 'get':
            return axios.get(
                url_endpoint,
                {
                    params: data,
                    headers: {
                        'etm-auth-key': state.getters.etm_auth_key
                    }
                }
            ).then((response) => response);
            // break
        case 'post':
            return axios.post(
                url_endpoint,
                data,
                {
                    headers: {
                        'etm-auth-key': state.getters.etm_auth_key
                    }
                }
            ).then((response) => response);
    }
}