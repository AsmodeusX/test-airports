import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'



Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        etm_auth_key: null,
    },
    mutations: {
        auth (state) {
            axios
                .get(`${process.env.VUE_APP_BASEURL}/api/login/${process.env.VUE_APP_ID}`)
                    .then(response => (state.etm_auth_key = response.data.etm_auth_key))
        }
    },
    actions: {
        auth (context) {
            context.commit('auth')
        }
    },
    getters: {
        etm_auth_key: state => {
            return state.etm_auth_key
        }
    }
})