import store from './../../store/index.js'


let isLoggedIn = store.getters.etm_auth_key !== null;
console.log(store.getters.etm_auth_key);

export default function auth({next, store }) {
    if (!isLoggedIn)
        store.dispatch('auth')
    isLoggedIn = store.getters.etm_auth_key !== null;
    return next()

}
